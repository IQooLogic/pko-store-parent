package rs.itcentar.pko.store.client;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.internal.util.Base64;
import rs.itcentar.pko.store.dto.Category;
import rs.itcentar.pko.store.dto.Product;
import rs.itcentar.pko.store.dto.Session;
import rs.itcentar.pko.store.dto.User;

/**
 *
 * @author milos
 */
public final class PKOStoreClient/* implements IPKOStoreClient*/ {

    private static final String BASE_URL = "http://localhost:8080/pko-store-api/api";
    private Client client = ClientBuilder.newClient();
    private Session session = Session.getInstance();// FIXME : methods should return errors too
    
    public User login(String username, String password) {
        WebTarget webTarget = client.target(BASE_URL).path("login");
        
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Form form = new Form();
        MultivaluedMap<String, String> store = form.asMap();
        store.add("username", username);
        store.add("password", password);
        Response response = invocationBuilder.post(Entity.form(new Form(store)));
        
        if(response.getStatus() == 200){
            User user = response.readEntity(User.class);
            response.close();
            return user;
        }
        
        return null;
    }
    
    public List<Category> getCategories() {
        WebTarget webTarget = client.target(BASE_URL).path("category");
        
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        if(response.getStatus() == 200){
            List<Category> categories = response.readEntity(new GenericType<List<Category>>(){});
            response.close();
            return categories;
        }
        
        return null;
    }
    
    public User getUserById(String id) {
        String username = session.getUser().getUsername();
        String password = session.getUser().getPassword();
        
        WebTarget webTarget = client.target(BASE_URL)
                .path("user").path(id);
        
        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);
        
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.get();
        if(response.getStatus() == 200){
            User user = response.readEntity(User.class);
            response.close();
            return user;
        }
        
        return null;
    }
    
    public String registerUser(User user) {
        WebTarget webTarget = client.target(BASE_URL).path("user");
        
        Response response = webTarget.request().put(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE));
        if(response.getStatus() == 201) {
            String location = response.getHeaderString("Location");
            String[] split = location.split("/");
            String id = split[split.length-1];
            response.close();
            return id;
        }
        
        return null;
    }
    
    public List<Product> getProducts(int offset, int pageSize) {
        String username = session.getUser().getUsername();
        String password = session.getUser().getPassword();

        WebTarget webTarget = client.target(BASE_URL).path("products").queryParam("offset", offset).queryParam("page_size", pageSize);

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.get();
        if (response.getStatus() == 200) {
            List<Product> products = response.readEntity(new GenericType<List<Product>>() {});
            response.close();
            return products;
        }
        
        return null;
    }
    
    public Product addProduct(Product product) {
        String username = session.getUser().getUsername();
        String password = session.getUser().getPassword();

        WebTarget webTarget = client.target(BASE_URL).path("products");

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.put(Entity.entity(product, MediaType.APPLICATION_JSON_TYPE));
        String id = response.readEntity(String.class);
        response.close();
        if(id != null){
            product.setId(id);
            return product;
        }
        return null;
    }
    
    public void deleteProduct(Product product) {
        String username = session.getUser().getUsername();
        String password = session.getUser().getPassword();

        WebTarget webTarget = client.target(BASE_URL).path("products").path(product.getId());

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.delete();
        response.close();
    }
    
    public void updateProduct(Product product) {
        String username = session.getUser().getUsername();
        String password = session.getUser().getPassword();

        WebTarget webTarget = client.target(BASE_URL).path("products").path(product.getId());

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.post(Entity.entity(product, MediaType.APPLICATION_JSON_TYPE));
        response.close();
    }
}
