package rs.itcentar.pko.store.client;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author milos
 */
public class OnlineChecker {

    public static boolean isAPIReachable() {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL("http://localhost:8080/pko-store-api/").openConnection();
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            
            return responseCode == 200;
        } catch (IOException ex) {
            return false;
        }
    }
}
