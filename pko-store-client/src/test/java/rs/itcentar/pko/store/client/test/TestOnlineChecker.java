package rs.itcentar.pko.store.client.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import rs.itcentar.pko.store.client.OnlineChecker;

/**
 *
 * @author milos
 */
public class TestOnlineChecker {
    
    @Ignore
    @Test
    public void testOnlineChecker(){
//        Assert.assertFalse(OnlineChecker.isAPIReachable());
        Assert.assertTrue(OnlineChecker.isAPIReachable());
    }
}
