package rs.itcentar.pko.store.dto;

public class Session {
    private User user;
    private String token;
    private static Session instance;

    private Session() {}
    
    public static Session getInstance() {
        if(instance == null) {
            instance = new Session();
        }
        
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
