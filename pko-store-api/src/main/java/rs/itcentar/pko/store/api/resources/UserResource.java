package rs.itcentar.pko.store.api.resources;

import java.net.URI;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import rs.itcentar.pko.store.api.ErrorCode;
import rs.itcentar.pko.store.api.ErrorResponse;
import rs.itcentar.pko.store.api.dal.repos.UsersRepo;
import rs.itcentar.pko.store.dto.User;

@Path("user")
public class UserResource {
    
    @RolesAllowed({"USER" , "ADMIN"})
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserById(@NotNull @PathParam("id") String id){
        if(id.isEmpty()) return Response.status(404).build();
        User user = UsersRepo.getUserById(id);
        if(user != null){
            return Response.ok(user).build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
    
    @PermitAll
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerUser(User user){
        if(user.getName().isEmpty() || user.getSurname().isEmpty() || user.getUsername().isEmpty() || user.getPassword().isEmpty()) return Response.status(404).build();
        if(!UsersRepo.exists(user.getUsername())){
            User newUser = UsersRepo.createUser(user.getName(), user.getSurname(), user.getUsername(), user.getPassword(), "USER");
            return Response.created(URI.create("user/" + newUser.getId())).build();
        }
        
        return ErrorResponse.newBuilder(ErrorCode.ENTITY_ALREADY_EXISTS, "Specified username is already registered").build();
    }
}
