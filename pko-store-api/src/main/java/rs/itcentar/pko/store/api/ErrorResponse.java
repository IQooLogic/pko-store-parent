package rs.itcentar.pko.store.api;

import java.util.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author milos
 */
public class ErrorResponse {

    public static Builder newBuilder(ErrorCode code, String message) {
        return new Builder(Response.Status.BAD_REQUEST, code, message);
    }

    public static Builder NotFoundBuilder() {
        return new Builder(Response.Status.NOT_FOUND, ErrorCode.GENERAL_ERROR, "Requested resource not found");
    }

    private int code;
    private String message;
    private final List<Error> errors = new ArrayList<>();

    public ErrorResponse(ErrorCode code, String message) {
        this.code = code.getCode();
        this.message = message;
    }

    public ErrorResponse(ErrorCode code, String message, Error[] errors) {
        this.code = code.getCode();
        this.message = message;
        this.errors.addAll(Arrays.asList(errors));
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Error> getErrors() {
        return Collections.unmodifiableList(errors);
    }

    public static class Error {

        private int code;
        private String message;

        public Error(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class Builder {

        private final Response.Status status;
        private final ErrorResponse item;

        public Builder(Response.Status httpStatus, ErrorCode code, String message) {
            this.status = httpStatus;
            this.item = new ErrorResponse(code, message);
        }

        public Builder addError(Error error) {
            this.item.errors.add(error);
            return this;
        }

        public Builder addErrors(Collection<Error> errors) {
            this.item.errors.addAll(errors);
            return this;
        }

        public Response build() {
            return Response.status(status).entity(this.item).type(MediaType.APPLICATION_JSON).build();
        }
    }
}
