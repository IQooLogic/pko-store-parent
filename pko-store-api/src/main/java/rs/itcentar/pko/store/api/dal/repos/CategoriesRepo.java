package rs.itcentar.pko.store.api.dal.repos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rs.itcentar.pko.store.api.dal.DBConnection;
import rs.itcentar.pko.store.dto.Category;

public class CategoriesRepo {
    
    public static List<Category> getCategories() {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                Statement statement = conn.createStatement();
                List<Category> ret;
                try (ResultSet rs = statement.executeQuery("SELECT * FROM categories;")) {
                    ret = new ArrayList<>();
                    while (rs.next()) {
                        int id = rs.getInt("idcategory");
                        String name = rs.getString("name");
                        Category b = new Category(id, name);
                        ret.add(b);
                    }
                }
                return ret;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoriesRepo.class.getName()).log(Level.SEVERE, null, ex);
            return Collections.EMPTY_LIST;
        }
    }
}
