package rs.itcentar.pko.store.api;

/**
 *
 * @author milos
 */
public enum ErrorCode {

    REQUIRED_QUERY_PARAMETER_MISSING(1),
    UNSUPPORTED_URL_SCHEME(2),
    URL_QUERY_COMPONENT_NOT_ALLOWED(3),
    URL_FRAGMENTS_NOT_ALLOWED(4),
    INVALID_URL_SYNTAX(5),
    REQUIRED_FORM_PARAMETER_MISSING(9),
    GENERAL_ERROR(10),
    AUTHENTICATION_ERROR(11),
    CAPTCHA_ERROR(12),
    ENTITY_ALREADY_EXISTS(13),
    REQUIRED_PATH_PARAMETER_MISSING(14),
    INVALID_REGISTRATION_TOKEN(15),
    INVALID_PASSWORD_TOKEN(17),
    PASSWORDS_NOT_MATCH(18),
    ICODE_ERROR(19),
    NO_INVITES_LEFT(20),
    PASSWORD_FAILS_COMPLEXITY_REQUIREMENTS(21),
    REQUIRED_ENTITY_MISSING(22);

    private final int code;

    private ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
