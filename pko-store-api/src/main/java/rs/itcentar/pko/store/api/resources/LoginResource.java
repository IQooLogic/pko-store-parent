package rs.itcentar.pko.store.api.resources;

import javax.annotation.security.PermitAll;
import javax.validation.constraints.NotNull;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import rs.itcentar.pko.store.api.dal.repos.UsersRepo;
import rs.itcentar.pko.store.dto.User;

@Path("login")
public class LoginResource {
    
    @PermitAll
    @POST
    @Produces(MediaType.APPLICATION_JSON)//Authorization: Basic YWRtaW46YWRtaW4=
    public Response login(@NotNull @FormParam("username") String username,
            @NotNull @FormParam("password") String password) {
        User user = UsersRepo.getUserByUsername(username);
        
        if(user != null) {
            if(password.equals(user.getPassword())) {
                return Response.ok(user).build();
            }
        }
        return Response.status(404).build();
    }
    
//    @RolesAllowed("USER")
//    @GET
//    @Produces(MediaType.TEXT_PLAIN)
//    public String logout() {
//        return "Got it!";
//    }
}
