package rs.itcentar.pko.store.api;

import java.util.logging.Logger;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import rs.itcentar.pko.store.api.filters.AuthenticationFilter;
import rs.itcentar.pko.store.api.resources.LoginResource;

public class PKOStoreApp extends ResourceConfig {
    private Logger logger = Logger.getGlobal();
    
    public PKOStoreApp() {
        packages("rs.itcentar.pko.store.api.resources");
        register(LoginResource.class);
        register(new LoggingFilter(logger, true));
        register(AuthenticationFilter.class);

//        property(ServerProperties.TRACING, "ALL");
    }
}
