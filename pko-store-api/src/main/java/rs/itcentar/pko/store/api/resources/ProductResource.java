package rs.itcentar.pko.store.api.resources;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import rs.itcentar.pko.store.api.ErrorCode;
import rs.itcentar.pko.store.api.ErrorResponse;
import rs.itcentar.pko.store.api.dal.repos.ProductsRepo;
import rs.itcentar.pko.store.dto.Product;

/**
 *
 * @author milos
 */
@Path("products")
public class ProductResource {

    private static final String TOTAL_PRODUCTS_HEADER = "X-TotalProducts";
    private static final String DEFAULT_PAGE_SIZE = "10";

    @RolesAllowed({"USER", "ADMIN"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = SerializationFeature.INDENT_OUTPUT,
            serializationDisable = SerializationFeature.FAIL_ON_EMPTY_BEANS)
    public Response list(@QueryParam("offset") @DefaultValue("0") int offset,
            @QueryParam("page_size") @DefaultValue(DEFAULT_PAGE_SIZE) int pageSize) {
        List<Product> products = ProductsRepo.getProducts(offset, pageSize);
        return Response.ok(products).header(TOTAL_PRODUCTS_HEADER, ProductsRepo.getCount()).build();
    }

    @RolesAllowed({"ADMIN"})
    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response addProduct(Product product) {
        if (product == null) {
            return ErrorResponse.newBuilder(ErrorCode.REQUIRED_ENTITY_MISSING, "Missing product information!").build();
        }
        String id = ProductsRepo.addProduct(product);
        if (!id.isEmpty()) {
            return Response.ok(id, MediaType.TEXT_PLAIN_TYPE).build();
        }
        return ErrorResponse.newBuilder(ErrorCode.GENERAL_ERROR, "Product not added!").build();
    }

    @RolesAllowed({"ADMIN"})
    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteProduct(@NotNull @PathParam("id") String id) {
        int deleteProduct = ProductsRepo.deleteProduct(id);
        if (deleteProduct >= 0) {
            return Response.ok().build();
        }
        return ErrorResponse.newBuilder(ErrorCode.GENERAL_ERROR, "Product not deleted!").build();
    }

    @RolesAllowed({"ADMIN"})
    @Path("/{id}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateProduct(@NotNull @PathParam("id") String id, Product product) {
        if (product == null) {
            return ErrorResponse.newBuilder(ErrorCode.REQUIRED_ENTITY_MISSING, "Missing product information!").build();
        }
        int updateProduct = ProductsRepo.updateProduct(product);
        if (updateProduct >= 0) {
            return Response.ok().build();
        }
        return ErrorResponse.newBuilder(ErrorCode.GENERAL_ERROR, "Product not updated!").build();
    }
}
