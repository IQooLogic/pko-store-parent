package rs.itcentar.pko.store.api.dal.repos;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import rs.itcentar.pko.store.api.dal.DBConnection;
import rs.itcentar.pko.store.dto.Category;
import rs.itcentar.pko.store.dto.Product;

/**
 *
 * @author milos
 */
public class ProductsRepo {

    /*
    SELECT products.*, categories.name AS category FROM products
    INNER JOIN categories
    ON products.categories_idcategory=categories.idcategory;
     */
    public static List<Product> getProducts(int offset, int pageSize) {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                PreparedStatement statement = conn.prepareStatement("SELECT products.*, categories.idcategory AS idcategory, categories.name AS category FROM products INNER JOIN categories ON products.categories_idcategory=categories.idcategory LIMIT ?,?;");
                statement.setInt(1, offset);
                statement.setInt(2, pageSize);
                ResultSet rs = statement.executeQuery();
                List<Product> products = new ArrayList<>();
                while (rs.next()) {
                    String id = rs.getString("idproduct");
                    String name = rs.getString("name");
                    BigDecimal price = rs.getBigDecimal("price");
                    String description = rs.getString("description");
                    int quantity = rs.getInt("quantity");
                    int idCategory = rs.getInt("idcategory");
                    String categoryName = rs.getString("category");
                    Category category = new Category(idCategory, categoryName);
                    Product product = new Product(id, name, price, description, quantity, category);
                    products.add(product);
                }
                return products;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepo.class.getName()).log(Level.SEVERE, null, ex);
            return Collections.EMPTY_LIST;
        }
    }

    public static int getCount() {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery("SELECT count(idproduct) AS total FROM products;");
                rs.next();
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepo.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public static String addProduct(Product product) {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                String id = UUID.randomUUID().toString();
                try (PreparedStatement ps = conn.prepareStatement("INSERT INTO products VALUES (?, ?, ?, ?, ?, ?)")) {
                    product.setId(id);

                    ps.setString(1, product.getId());
                    ps.setString(2, product.getName());
                    ps.setBigDecimal(3, product.getPrice());
                    ps.setString(4, product.getDescription());
                    ps.setInt(5, product.getQuantity());
                    ps.setInt(6, product.getCategory().getId());
                    int ret = ps.executeUpdate();
                }
                return id;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepo.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    public static int deleteProduct(String id) {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                int ret;
                try (PreparedStatement ps = conn.prepareStatement("DELETE FROM products WHERE idproduct = ?")) {
                    ps.setString(1, id);
                    ret = ps.executeUpdate();
                    System.out.println("RET: " + ret);
                }
                return ret;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepo.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public static int updateProduct(Product product) {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                int ret;
                try (PreparedStatement ps = conn.prepareStatement("UPDATE products SET name = ?, price = ?, description = ?, quantity = ?, categories_idcategory = ? WHERE idproduct = ?")) {
                    ps.setString(1, product.getName());
                    ps.setBigDecimal(2, product.getPrice());
                    ps.setString(3, product.getDescription());
                    ps.setInt(4, product.getQuantity());
                    ps.setInt(5, product.getCategory().getId());
                    ps.setString(6, product.getId());
                    ret = ps.executeUpdate();
                }
                return ret;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepo.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
}
