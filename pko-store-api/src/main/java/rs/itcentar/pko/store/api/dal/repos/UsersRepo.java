package rs.itcentar.pko.store.api.dal.repos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import rs.itcentar.pko.store.api.dal.DBConnection;
import rs.itcentar.pko.store.dto.User;

public class UsersRepo {
    
    public static User getUserById(String id){
        try {
            try (Connection conn = DBConnection.getConnection()) {
                PreparedStatement statement = conn.prepareStatement("SELECT * FROM users WHERE iduser = ? LIMIT 1;");
                statement.setString(1, id);
                ResultSet rs = statement.executeQuery();
                User user = null;
                while (rs.next()) {
                    String uid = rs.getString("iduser");
                    String name = rs.getString("name");
                    String surname = rs.getString("surname");
                    String username = rs.getString("username");
                    String pasword = rs.getString("password");
                    String role = rs.getString("role");
                    user = new User(uid, name, surname, username, pasword, role);
                }
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepo.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static User getUserByUsername(String username){
        try {
            try (Connection conn = DBConnection.getConnection()) {
                PreparedStatement statement = conn.prepareStatement("SELECT * FROM users WHERE username = ? LIMIT 1;");
                statement.setString(1, username);
                ResultSet rs = statement.executeQuery();
                User user = null;
                while (rs.next()) {
                    String uid = rs.getString("iduser");
                    String name = rs.getString("name");
                    String surname = rs.getString("surname");
//                    String username = rs.getString("username");
                    String pasword = rs.getString("password");
                    String role = rs.getString("role");
                    user = new User(uid, name, surname, username, pasword, role);
                }
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepo.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static User createUser(String name, String surname, String username, String password, String role){
        User user = new User(UUID.randomUUID().toString(), name, surname, username, password, role);
        
        try {
            try (Connection conn = DBConnection.getConnection()) {
                int ret;
                try (PreparedStatement ps = conn.prepareStatement("INSERT INTO users(iduser, name, surname, username, password, role) VALUES (?, ?, ?, ?, ?, ?)")) {
                    ps.setString(1, user.getId());
                    ps.setString(2, user.getName());
                    ps.setString(3, user.getSurname());
                    ps.setString(4, user.getUsername());
                    ps.setString(5, user.getPassword());
                    ps.setString(6, user.getRole());
                    ret = ps.executeUpdate();
                }
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepo.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static int deleteUser(String id){
        try {
            try (Connection conn = DBConnection.getConnection()) {
                int ret;
                try (PreparedStatement ps = conn.prepareStatement("DELETE FROM users WHERE iduser = ?")) {
                    ps.setString(1, id);
                    ret = ps.executeUpdate();
                }
                return ret;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepo.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    
    public static boolean exists(String username) {
        try {
            try (Connection conn = DBConnection.getConnection()) {
                PreparedStatement statement = conn.prepareStatement("SELECT username FROM users WHERE username = ?;");
                statement.setString(1, username);
                ResultSet rs = statement.executeQuery();
                return rs.next();
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
