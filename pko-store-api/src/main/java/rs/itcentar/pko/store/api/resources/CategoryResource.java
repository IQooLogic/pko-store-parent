package rs.itcentar.pko.store.api.resources;

import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import rs.itcentar.pko.store.api.dal.repos.CategoriesRepo;
import rs.itcentar.pko.store.dto.Category;

@Path("category")
public class CategoryResource {
    
    @PermitAll
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategories(){
        List<Category> categories = CategoriesRepo.getCategories();
        if(categories != null){
            return Response.ok(categories).build();
        }
        return Response.status(404).build();
    }
}
