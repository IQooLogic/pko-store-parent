package rs.itcentar.pko.store.test;

import java.math.BigDecimal;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.internal.util.Base64;
import org.junit.Assert;
import org.junit.Test;
import rs.itcentar.pko.store.api.dal.repos.CategoriesRepo;
import rs.itcentar.pko.store.dto.Category;
import rs.itcentar.pko.store.dto.Product;

/**
 *
 * @author milos
 */
public class TestProductResource {

    @Test
    public void testGetProducts() {
        System.out.println("TEST GET PRODUCTS");
        String username = "milos";
        String password = "123";

        ClientConfig config = new ClientConfig();
//        config.register(LoggingFilter.class);
//        config.register(AuthenticationFilter.class);
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api").path("products").queryParam("offset", 0).queryParam("page_size", 10);

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.get();
        Assert.assertEquals(200, response.getStatus());
        if (response.getStatus() == 200) {
            List<Product> products = response.readEntity(new GenericType<List<Product>>() {});
            if (products != null) {
                System.out.println(products);
                Assert.assertTrue("Products are empty!", !products.isEmpty());
            } else {
                Assert.fail("Server can not find categories!");
            }
        }
    }

    @Test
    public void testAddProduct() {
        System.out.println("TEST ADD PRODUCT");

        String username = "milos";
        String password = "123";

        List<Category> categories = CategoriesRepo.getCategories();
        Product p = new Product("CPU LGA2011-v3 Intel® Core™ i7-5820K", new BigDecimal(50799), "", 10, categories.get(1));

        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api").path("products");

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.put(Entity.entity(p, MediaType.APPLICATION_JSON_TYPE));
        Assert.assertEquals(200, response.getStatus());
        String id = response.readEntity(String.class);
        System.out.println(String.format("Product created with id: %s", id));
        p.setId(id);
        Assert.assertTrue("Product id is empty!", !id.isEmpty());

        testUpdateProduct(p);
        testDeleteProduct(p);
    }

//    @Test
    public void testUpdateProduct(Product p) {
        System.out.println("TEST UPDATE PRODUCT");
        String username = "milos";
        String password = "123";
        
        p.setQuantity(20);

        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api").path("products").path(p.getId());

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.post(Entity.entity(p, MediaType.APPLICATION_JSON_TYPE));
        Assert.assertEquals(200, response.getStatus());
    }

//    @Test
    public void testDeleteProduct(Product p) {
        System.out.println("TEST DELETE PRODUCT");
        String username = "milos";
        String password = "123";
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api").path("products").path(p.getId());

        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.delete();
        Assert.assertEquals(200, response.getStatus());
    }
}
