package rs.itcentar.pko.store.test;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.internal.util.Base64;
import org.junit.Assert;
import org.junit.Test;
import rs.itcentar.pko.store.dto.Category;

public class TestCategoryResource {
    
    @Test
    public void testGetCategories(){
        System.out.println("TEST GET CATEGORIES");
        String username = "milos";
        String password = "123";
        
        ClientConfig config = new ClientConfig();
//        config.register(LoggingFilter.class);
//        config.register(AuthenticationFilter.class);
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api").path("category");
        
        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);
        
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.get();
        Assert.assertEquals(200, response.getStatus());
        if(response.getStatus() == 200){
            List<Category> categories = response.readEntity(new GenericType<List<Category>>(){});
            if(categories != null){
                System.out.println(categories);
                Assert.assertTrue("Categories are empty!", !categories.isEmpty());
            } else {
                Assert.fail("Server can not find categories!");
            }
        }
    }
}
