package rs.itcentar.pko.store.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.Assert;
import org.junit.Test;
import rs.itcentar.pko.store.dto.User;

public class TestLoginResource {
    
    @Test
    public void testLogin(){
        System.out.println("TEST LOGIN");
        ClientConfig config = new ClientConfig();
//        config.register(LoggingFilter.class);
//        config.register(AuthenticationFilter.class);
        Client client = ClientBuilder.newClient(config);
//        WebTarget webTarget = client.target("http://localhost:8080/pko-store-api/api").path("login");
        WebTarget webTarget = client.target("http://localhost:8080/api").path("login");// jetty
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic bWlsb3M6MTIz");
        Form form = new Form();
        MultivaluedMap<String, String> store = form.asMap();
        store.add("username", "milos");
        store.add("password", "123");
        Response response = invocationBuilder.post(Entity.form(new Form(store)));
        
        if(response.getStatus() == 200){
            User user = response.readEntity(User.class);
        } else {
            Assert.fail(String.format("Server responded with %d should be 200!", response.getStatus()));
        }
        
        Assert.assertEquals(200, response.getStatus());
    }
}
