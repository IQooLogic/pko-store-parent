package rs.itcentar.pko.store.test;

import java.util.UUID;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.internal.util.Base64;
import org.junit.Assert;
import org.junit.Test;
import rs.itcentar.pko.store.api.dal.repos.UsersRepo;
import rs.itcentar.pko.store.dto.User;

public class TestUserResource {
    
    @Test
    public void testGetUser(){
        System.out.println("TEST GET USER");
        String username = "milos";
        String password = "123";
        
        ClientConfig config = new ClientConfig();
//        config.register(LoggingFilter.class);
//        config.register(AuthenticationFilter.class);
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api")
                .path("user").path("c245d4f6-eea7-11e5-9bd2-74d02b90bc2f");
        
        String user_pass_base64 = Base64.encodeAsString(username + ":" + password);
        
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION.toLowerCase(), "Basic " + user_pass_base64);
        Response response = invocationBuilder.get();
        Assert.assertEquals(200, response.getStatus());
        if(response.getStatus() == 200){
            User user = response.readEntity(User.class);
            Assert.assertEquals("Milos", user.getName());
            Assert.assertEquals("Stojkovic", user.getSurname());
        } else {
            Assert.fail(String.format("Server responded with %d should be 200!", response.getStatus()));
        }
    }
    
    @Test
    public void testRegisterUser(){
        //{"id":"c245d4f6-eea7-11e5-9bd2-74d02b90bc2f","name":"Milos","surname":"Stojkovic","username":"milos","password":"123","role":"ADMIN"}
        System.out.println("TEST REGISTER USER");
        ClientConfig config = new ClientConfig();
//        config.register(LoggingFilter.class);
//        config.register(AuthenticationFilter.class);
        Client client = ClientBuilder.newClient(config);
        WebTarget webTarget = client.target("http://localhost:8080/api").path("user");
        User user = new User(UUID.randomUUID().toString(), "TestName", "TestSurname", "test", "test", "USER");
        Response response = webTarget.request().put(Entity.entity(user, MediaType.APPLICATION_JSON_TYPE));
        Assert.assertEquals(201, response.getStatus());
        if(response.getStatus() == 201) {
//            User usr = response.readEntity(User.class);
            String location = response.getHeaderString("Location");
            String[] split = location.split("/");
            String id = split[split.length-1];
            User usr = UsersRepo.getUserById(id);
            Assert.assertEquals("TestName", usr.getName());
            Assert.assertEquals("TestSurname", usr.getSurname());
            
            int ret = UsersRepo.deleteUser(id);
            Assert.assertEquals(1, ret);
        } else {
            Assert.fail(String.format("Server responded with %d should be 200!", response.getStatus()));
        }
    }
}
