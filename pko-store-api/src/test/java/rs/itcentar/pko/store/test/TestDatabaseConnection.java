package rs.itcentar.pko.store.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.Test;
import rs.itcentar.pko.store.api.dal.DBConnection;

public class TestDatabaseConnection {
    
    @Test
    public void testDBConnection(){
        System.out.println("TEST DB CONNECTION");
        try {
            Connection connection = DBConnection.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM users;");
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }
}
