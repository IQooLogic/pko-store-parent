-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2016 at 03:29 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pko-store`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `idcategory` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`idcategory`, `name`) VALUES
(1, 'MOTHERBOARD'),
(2, 'CPU'),
(3, 'RAM'),
(4, 'GRAPHICS CARD'),
(5, 'SSD'),
(6, 'HARDDRIVE'),
(7, 'PSU'),
(8, 'CASE');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idproduct` varchar(36) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `description` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `categories_idcategory` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`idproduct`, `name`, `price`, `description`, `quantity`, `categories_idcategory`) VALUES
('d8833b80-f35e-11e5-b663-74d02b90bc2f', 'MB AM3+ 970 Gigabyte GA-970A-DS3P', '8969.00', 'Model: GA-970A-DS3P\r\nFormat ploče: ATX\r\nČipset: North Bridge AMD 970, South Bridge AMD SB950\r\nSlot/Socket: AM3+/AM3\r\nPodržani procesori: AMD AM3+ FX procesori, AM3 Phenom II i Athlon II procesori', 10, 1),
('f2fff9e8-f35e-11e5-b663-74d02b90bc2f', 'MB LGA1151 Z170 Gigabyte GA-Z170X-Gaming 3', '20899.00', 'Model: GA-Z170X-Gaming 3\r\nFormat ploče: ATX\r\nČipset: Intel Z170\r\nSlot/Socket: LGA1151\r\nPodržani procesori: Intel Core i7 / Core i5 / Core i3 / Pentium / Celeron', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `idpurchase` varchar(36) NOT NULL,
  `timestamp` mediumtext NOT NULL,
  `users_iduser` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchases_has_products`
--

CREATE TABLE `purchases_has_products` (
  `idproduct` varchar(36) NOT NULL,
  `idpurchase` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iduser` varchar(36) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iduser`, `name`, `surname`, `username`, `password`, `role`) VALUES
('c245d4f6-eea7-11e5-9bd2-74d02b90bc2f', 'Milos', 'Stojkovic', 'milos', '123', 'ADMIN'),
('c245e7e4-eea7-11e5-9bd2-74d02b90bc2f', 'Pera', 'Peric', 'pera', '123', 'USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`idcategory`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`idproduct`),
  ADD KEY `fk_products_categories_idx` (`categories_idcategory`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`idpurchase`,`users_iduser`),
  ADD KEY `fk_purchases_users_idx` (`users_iduser`);

--
-- Indexes for table `purchases_has_products`
--
ALTER TABLE `purchases_has_products`
  ADD PRIMARY KEY (`idproduct`,`idpurchase`),
  ADD KEY `fk_purchases_has_products_products_idx` (`idproduct`),
  ADD KEY `fk_purchases_has_products_purchases_idx` (`idpurchase`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `idcategory` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_categories` FOREIGN KEY (`categories_idcategory`) REFERENCES `categories` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `fk_purchases_users1` FOREIGN KEY (`users_iduser`) REFERENCES `users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchases_has_products`
--
ALTER TABLE `purchases_has_products`
  ADD CONSTRAINT `fk_purchases_has_products_products1` FOREIGN KEY (`idproduct`) REFERENCES `products` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchases_has_products_purchases1` FOREIGN KEY (`idpurchase`) REFERENCES `purchases` (`idpurchase`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
