package rs.itcentar.pko.store.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import rs.itcentar.pko.store.dto.Category;

/**
 *
 * @author milos
 */
public class ProductsTableModel extends DefaultTableModel {

    private ProductsModel model = ProductsModel.getInstance();
    private List<ProductWrapper> products = new ArrayList<>();
    private List<ProductWrapper> filteredPproducts = new ArrayList<>();

    public ProductsTableModel() {
//        model.addPropertyChangeListener(new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//                switch (evt.getPropertyName()) {
//                    case ProductsModel.PROP_MODEL_FILLED:
//                    case ProductsModel.PROP_PRODUCT_ADDED: {
//                        try {
//                            update(model.getProducts());
//                        } catch (InterruptedException ex) {
//                            Logger.getLogger(ProductsTableModel.class.getName()).log(Level.SEVERE, null, ex);
//                        } catch (ExecutionException ex) {
//                            Logger.getLogger(ProductsTableModel.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                    break;
//                }
//            }
//        });
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        ProductWrapper p = this.filteredPproducts.get(row);
        switch (column) {
            case 0:
                p.setName((String) value);
                break;
            case 1:
                p.setPrice((BigDecimal) value);
                break;
            case 2:
                p.setDescription((String) value);
                break;
            case 3:
                p.setQuantity((int) value);
                break;
            case 4:
                p.setCategory((Category) value);
                break;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        ProductWrapper p = this.filteredPproducts.get(row);
        return p;
//        switch (column) {
//            case 0:
//                return p.getName();
//            case 1:
//                return p.getPrice();
//            case 2:
//                return p.getDescription();
//            case 3:
//                return p.getQuantity();
//            case 4:
//                if(p.getCategory() == null) return "null";
//                return p.getCategory().getName();
//            default:
//                return p;
//        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return model.isEditMode();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Name";
            case 1:
                return "Price";
            case 2:
                return "Description";
            case 3:
                return "Quantity";
            case 4:
                return "Category";
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return ProductWrapper.class;
//        switch (column) {
//            case 0:
//                return String.class;
//            case 1:
//                return BigDecimal.class;
//            case 2:
//                return String.class;
//            case 3:
//                return Integer.class;
//            case 4:
//                return Category.class;
//            default:
//                return Object.class;
//        }
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return this.filteredPproducts == null ? 0 : this.filteredPproducts.size();
    }

    public ProductWrapper getProduct(int row) {
        return this.filteredPproducts.get(row);
    }

    public List<ProductWrapper> getProducts() {
        return this.products;
    }

    public void update(List<ProductWrapper> products) {
        this.products = products;
        this.filteredPproducts = products;
        fireTableDataChanged();
    }

    public void filterUpdate(List<ProductWrapper> products) {
        this.filteredPproducts = products;
        fireTableDataChanged();
    }
}
