package rs.itcentar.pko.store.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import rs.itcentar.pko.store.dto.Category;

/**
 *
 * @author milos
 */
public class AdminFrame extends javax.swing.JFrame {

    private LoginFrame parent;
    private CategoriesListModel categoriesModel;
    private CategoriesListModel editorCategoriesModel;
    private ProductsTableModel productsModel = new ProductsTableModel();
    private ProductsModel model = ProductsModel.getInstance();
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public AdminFrame(LoginFrame parent) {
        this.parent = parent;
        initComponents();
        
        initPopupMenus();

        try {
            model.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    switch (evt.getPropertyName()) {
                        case ProductsModel.PROP_MODEL_FILLED:
                        case ProductsModel.PROP_PRODUCT_ADDED: {
                            try {
                                productsModel.update(model.getProducts());
                            } catch (InterruptedException ex) {
                                Logger.getLogger(ProductsTableModel.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ExecutionException ex) {
                                Logger.getLogger(ProductsTableModel.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;
                        case ProductsModel.PROP_EDITMODE_CHANGED:
                            initPopupMenus();
                            break;
                    }
                }
            });
            categoriesModel = new CategoriesListModel(model.getCategories());
            cbCategory.setModel(categoriesModel);
            cbCategory.setRenderer(new CategoryListCellRenderer());

            productsModel.update(model.getProducts());
            tProducts.setModel(productsModel);

            TableColumn categoryColumn = tProducts.getColumnModel().getColumn(4);
            JComboBox<Category> cb = new JComboBox();
            editorCategoriesModel = new CategoriesListModel(model.getCategories());
            cb.setModel(editorCategoriesModel);
            cb.setRenderer(new CategoryListCellRenderer());
            categoryColumn.setCellEditor(new DefaultCellEditor(cb));

            executor.shutdown();
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        cbCategory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<ProductWrapper> products = productsModel.getProducts();
                List<ProductWrapper> filteredProducts = new ArrayList<>();
                for(ProductWrapper p : products) {
                    if(p.getCategory().getName().equalsIgnoreCase(((Category) cbCategory.getSelectedItem()).getName())) {
                        filteredProducts.add(p);
                    }
                }
                productsModel.filterUpdate(filteredProducts);
            }
        });

        TableRowSorter<ProductsTableModel> sorter = new TableRowSorter<>(productsModel);
        tfFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filter();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filter();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filter();
            }

            private void filter() {
                RowFilter<ProductsTableModel, Integer> filter = new RowFilter<ProductsTableModel, Integer>() {

                    @Override
                    public boolean include(RowFilter.Entry<? extends ProductsTableModel, ? extends Integer> entry) {
                        ProductWrapper product = productsModel.getProduct(entry.getIdentifier());
                        return product.getName().contains(tfFilter.getText());
                    }
                };
                sorter.setRowFilter(filter);
                tProducts.setRowSorter(sorter);
            }
        });
        
        tProducts.setDefaultRenderer(ProductWrapper.class, new ProductsTableRenderer());
        tProducts.setFillsViewportHeight(true);
        tProducts.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                rightClick(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {}

            @Override
            public void mouseReleased(MouseEvent e) {}

            @Override
            public void mouseEntered(MouseEvent e) {}

            @Override
            public void mouseExited(MouseEvent e) {}
            
            private void rightClick(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    int row = tProducts.rowAtPoint(e.getPoint());
                    int column = tProducts.columnAtPoint(e.getPoint());
//                    selectedRow = tProducts.getSelectedRow();
                    if (!tProducts.isRowSelected(row)) {
                        if (row > -1) {
                            tProducts.setRowSelectionInterval(row, row);
                        }
                        tProducts.changeSelection(row, column, false, false);
                    }
                    pmProduct.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        
        miAddProduct.setAction(new AddProductAction(miAddProduct.getText()));
        cbmiToggleEditable.setAction(new ToggleEditableAction(cbmiToggleEditable.getText()));

//        SwingWorker swCategory = new SwingWorker<List<Category>, Void>() {
//            @Override
//            protected List<Category> doInBackground() throws Exception {
//                return client.getCategories();
//            }
//
//            @Override
//            protected void done() {
//                try {
//                    List<Category> categories = get();
//                    categoriesModel = new CategoriesListModel(categories);
//                    cbCategory.setModel(categoriesModel);
//                    cbCategory.setRenderer(new CategoryListCellRenderer());
//                } catch (InterruptedException | ExecutionException ex) {
//                    Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        };
//        swCategory.execute();
//        
//        SwingWorker swProducts = new SwingWorker<List<Product>, Void>() {
//            @Override
//            protected List<Product> doInBackground() throws Exception {
//                return client.getProducts(0, 10);
//            }
//
//            @Override
//            protected void done() {
//                try {
//                    List<Product> products = get();
//                    productsModel.update(products);
//                    tProducts.setModel(productsModel);
//                    
//                    TableColumn categoryColumn = tProducts.getColumnModel().getColumn(4);
//                    JComboBox<Category> cb = new JComboBox();
//                    cb.setModel(categoriesModel);
//                    cb.setRenderer(new CategoryListCellRenderer());
//                    categoryColumn.setCellEditor(new DefaultCellEditor(cb));
//                } catch (InterruptedException | ExecutionException ex) {
//                    Logger.getLogger(AdminFrame.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        };
//        swProducts.execute();
    }
    
    private void initPopupMenus() {
        pmProduct.removeAll();
        if(model.isEditMode()) {
            pmProduct.add(miAddProduct);
            pmProduct.add(miDeleteProduct);
            pmProduct.add(separator);
            pmProduct.add(cbmiToggleEditable);
        } else {
            pmProduct.add(cbmiToggleEditable);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pmProduct = new javax.swing.JPopupMenu();
        miAddProduct = new javax.swing.JMenuItem();
        miDeleteProduct = new javax.swing.JMenuItem();
        separator = new javax.swing.JPopupMenu.Separator();
        cbmiToggleEditable = new javax.swing.JCheckBoxMenuItem();
        jLabel1 = new javax.swing.JLabel();
        tfFilter = new javax.swing.JTextField();
        cbCategory = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tProducts = new javax.swing.JTable();
        bLast = new javax.swing.JButton();
        bNext = new javax.swing.JButton();
        bFirst = new javax.swing.JButton();
        bPrev = new javax.swing.JButton();
        bUpdate = new javax.swing.JButton();
        bReset = new javax.swing.JButton();

        miAddProduct.setText("Add Product");
        pmProduct.add(miAddProduct);

        miDeleteProduct.setText("Delete Product");
        pmProduct.add(miDeleteProduct);
        pmProduct.add(separator);

        cbmiToggleEditable.setText("Editable");
        pmProduct.add(cbmiToggleEditable);

        setTitle("PKO Store - Admin Frame");
        setMinimumSize(new java.awt.Dimension(869, 488));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Filter:");

        jLabel2.setText("Category:");

        tProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tProducts.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tProducts);

        bLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/last.png"))); // NOI18N

        bNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/next.png"))); // NOI18N

        bFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/first.png"))); // NOI18N

        bPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/prev.png"))); // NOI18N

        bUpdate.setText("Update");

        bReset.setText("Reset");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfFilter)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(bUpdate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bReset))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 789, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bLast, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bNext, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bPrev, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bFirst, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bLast)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bNext)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bPrev)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bFirst)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bUpdate)
                    .addComponent(bReset))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.setVisible(false);
        this.parent.setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bFirst;
    private javax.swing.JButton bLast;
    private javax.swing.JButton bNext;
    private javax.swing.JButton bPrev;
    private javax.swing.JButton bReset;
    private javax.swing.JButton bUpdate;
    private javax.swing.JComboBox<Category> cbCategory;
    private javax.swing.JCheckBoxMenuItem cbmiToggleEditable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem miAddProduct;
    private javax.swing.JMenuItem miDeleteProduct;
    private javax.swing.JPopupMenu pmProduct;
    private javax.swing.JPopupMenu.Separator separator;
    private javax.swing.JTable tProducts;
    private javax.swing.JTextField tfFilter;
    // End of variables declaration//GEN-END:variables
}
