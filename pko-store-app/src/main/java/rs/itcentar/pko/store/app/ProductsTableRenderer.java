package rs.itcentar.pko.store.app;

import java.awt.Color;
import java.awt.Component;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import rs.itcentar.pko.store.dto.Category;

/**
 *
 * @author milos
 */
public class ProductsTableRenderer implements TableCellRenderer {

    private ProductsModel model = ProductsModel.getInstance();
    private CategoriesListModel categoriesModel;
    private JLabel label = new JLabel();

    public ProductsTableRenderer() {}

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        ProductWrapper pw = ((ProductWrapper) value);
        if (pw != null) {
            switch (column) {
                case 0:// name
                    drawColors(label, table, pw, isSelected, hasFocus, row, column);
                    label.setText(pw.getName());
                    return label;
                case 1:// price
                    drawColors(label, table, pw, isSelected, hasFocus, row, column);
                    label.setText(pw.getPrice().toString());
                    return label;
                case 2: // description
                    drawColors(label, table, pw, isSelected, hasFocus, row, column);
                    label.setText(pw.getDescription());
                    return label;
                case 3: // quantity
                    drawColors(label, table, pw, isSelected, hasFocus, row, column);
                    label.setText(String.valueOf(pw.getQuantity()));
                    return label;
                case 4: // category
                    JComboBox<Category> cbCategory = setupCheckBox();
                    drawColors(setupCheckBox(), table, pw, isSelected, hasFocus, row, column);
                    int index = categoriesModel.getIndexForCategory(pw.getCategory());
                    cbCategory.setSelectedIndex(index);
                    return cbCategory;
                default:
                    return new JLabel("empty");
            }
        }
        return new JLabel("empty");
    }

    private void drawColors(Component comp, JTable table, ProductWrapper pw, boolean isSelected, boolean hasFocus, int row, int column) {
        JComboBox<Category> cb;
        JLabel l;
        if (comp instanceof JComboBox) {
            cb = (JComboBox) comp;
            cb.setOpaque(true);
            cb.setBorder(new EmptyBorder(0, 0, 0, 0));
            if (isSelected) {
                cb.setForeground(table.getSelectionForeground());
                cb.setBackground(table.getSelectionBackground());
            } else {
                cb.setForeground(table.getForeground());
                cb.setBackground(table.getBackground());
            }
            if (hasFocus) {
                if (table.isCellEditable(row, column)) {
                    cb.setForeground(UIManager.getColor("Table.focusCellForeground"));
                    cb.setBackground(UIManager.getColor("Table.focusCellBackground"));
                }
            } else {
                cb.setBorder(new EmptyBorder(0, 0, 0, 0));
            }
        } else {
            l = (JLabel) comp;
            l.setOpaque(true);
            l.setBorder(new EmptyBorder(0, 0, 0, 0));
            if (isSelected) {
                l.setForeground(table.getSelectionForeground());
                l.setBackground(table.getSelectionBackground());
            } else {
                if(pw.isDelete()) {
                    l.setForeground(table.getForeground());
                    l.setBackground(Color.LIGHT_GRAY);
                } else if(pw.isDirty()) {
                    l.setForeground(table.getForeground());
                    l.setBackground(Color.RED);
                } else {
                    l.setForeground(table.getForeground());
                    l.setBackground(table.getBackground());
                }
            }
            if (!hasFocus) {
                l.setBorder(new EmptyBorder(0, 0, 0, 0));
            }
        }
    }
    
    private JComboBox<Category> setupCheckBox(){
        try {
            categoriesModel = new CategoriesListModel(model.getCategories());
            JComboBox<Category> cbCategory = new JComboBox();
            cbCategory.setModel(categoriesModel);
            cbCategory.setRenderer(new CategoryListCellRenderer());
            
            return cbCategory;
        } catch (InterruptedException ex) {
            Logger.getLogger(ProductsTableRenderer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(ProductsTableRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
