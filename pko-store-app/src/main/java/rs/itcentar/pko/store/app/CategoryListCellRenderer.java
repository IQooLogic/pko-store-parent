package rs.itcentar.pko.store.app;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import rs.itcentar.pko.store.dto.Category;

/**
 *
 * @author milos
 */
public class CategoryListCellRenderer extends JLabel implements ListCellRenderer<Category> {

    public CategoryListCellRenderer() {
        this.setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Category category, int index, boolean isSelected, boolean cellHasFocus) {
        if(category != null) {
            setText(category.getName());// FIXME : add icons
        }
        
        if(isSelected){
//            this.setBackground(Color.decode("#D0ECEA"));
            this.setBackground(Color.decode("#39698A"));
            this.setForeground(Color.white);
        } else {
//            this.setBackground(Color.decode("#9FD6D2"));
            this.setBackground(Color.white);
            this.setForeground(Color.black);
        }
        
        return this;
    }
    
}
