package rs.itcentar.pko.store.app;

import java.util.List;
import java.util.concurrent.Callable;
import rs.itcentar.pko.store.client.PKOStoreClient;
import rs.itcentar.pko.store.dto.Product;

/**
 *
 * @author milos
 */
public class ProductsCallable implements Callable<List<Product>> {
    private PKOStoreClient client = new PKOStoreClient();
    private int offset = 0;
    private int pageSize = 0;

    public ProductsCallable(int offset, int pageSize) {
        this.offset = offset;
        this.pageSize = pageSize;
    }
    
    @Override
    public List<Product> call() throws Exception {
        return client.getProducts(this.offset, this.pageSize);
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
