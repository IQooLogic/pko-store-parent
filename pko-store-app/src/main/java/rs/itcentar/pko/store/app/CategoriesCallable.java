package rs.itcentar.pko.store.app;

import java.util.List;
import java.util.concurrent.Callable;
import rs.itcentar.pko.store.client.PKOStoreClient;
import rs.itcentar.pko.store.dto.Category;

/**
 *
 * @author milos
 */
public class CategoriesCallable implements Callable<List<Category>> {
    private PKOStoreClient client = new PKOStoreClient();
    
    @Override
    public List<Category> call() throws Exception {
        return client.getCategories();
    }
}
