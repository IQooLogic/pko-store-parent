package rs.itcentar.pko.store.app;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author milos
 */
public class AddProductAction extends AbstractAction {
    private ProductsModel model = ProductsModel.getInstance();

    public AddProductAction(String name) {
        super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        model.addNewProduct();
    }
}
