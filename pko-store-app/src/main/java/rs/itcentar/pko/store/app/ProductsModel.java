package rs.itcentar.pko.store.app;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import rs.itcentar.pko.store.dto.Category;
import rs.itcentar.pko.store.dto.Product;

/**
 *
 * @author milos
 */
public class ProductsModel implements PropertyChangeListener {
    public static final String PROP_EDITMODE_CHANGED = "editModeChanged";
    public static final String PROP_PRODUCTS_PERSISTED = "productsPersisted";
    public static final String PROP_MODEL_FILLED = "modelFilled";
    public static final String PROP_PRODUCT_ADDED = "productAdded";
    public static final String PROP_PRODUCT_REMOVED = "productRemoved";
    public static final String PROP_PRODUCT_MARKED_AS_DIRTY = "productMarkedAsDirty";
    public static final String PROP_PRODUCT_MARKED_FOR_DELETE = "productMarkedForDelete";
    
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private static ProductsModel instance = null;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private List<Category> categories = new ArrayList<>();
    private List<ProductWrapper> backupProducts = new ArrayList<>();
    private List<ProductWrapper> usedPoducts = new ArrayList<>();
    private CategoriesCallable categoriesCallable = null;
    private ProductsCallable productsCallable = null;
    private boolean editMode = false;
    
    private ProductsModel(){}
    
    public static ProductsModel getInstance() {
        if(instance == null) {
            instance = new ProductsModel();
        }
        
        return instance;
    }
    
    public void fillModel(List<Product> products) {
        for(Product p : products) {
            ProductWrapper bpw = new ProductWrapper(p);
            backupProducts.add(bpw);
            
            ProductWrapper upw = new ProductWrapper(p);
            usedPoducts.add(upw);
        }
        
        pcs.firePropertyChange(PROP_MODEL_FILLED, null, usedPoducts);
    }
    
    public List<ProductWrapper> getProductWrappers() {
        return usedPoducts;
    }
    
    public List<Category> getCategories() throws InterruptedException, ExecutionException {
        if(categories == null || categories.isEmpty()) {
            categoriesCallable = new CategoriesCallable();
            Future<List<Category>> futureCategories = executor.submit(categoriesCallable);
            categories = futureCategories.get();
        }
        
        return categories;
    }
    
    public List<ProductWrapper> getProducts() throws InterruptedException, ExecutionException {
        if(usedPoducts == null || usedPoducts.isEmpty()) {
            productsCallable = new ProductsCallable(0, 10);
            Future<List<Product>> futureProducts = executor.submit(productsCallable);
            usedPoducts = productListToProductWrapperList(futureProducts.get());
        }
        
//        return productWrapperListToProductList(usedPoducts);
        return usedPoducts;
    }
    
    public ProductWrapper getProductById(String id) {
        for(ProductWrapper wp : usedPoducts){
            if(id.equals(wp.getId())) {
                return wp;
            }
        }
        
        return null;
    }
    
    public ProductWrapper getProductByIndex(int index) {
        ProductWrapper wp = this.usedPoducts.get(index);
        return wp;
    }
    
    public List<Product> getProductsByCategory(Category category) {
        List<Product> productsByCategory = new ArrayList<>();
        for(ProductWrapper wp : this.usedPoducts) {
            if(category.getName().equals(wp.getCategory().getName())){
                productsByCategory.add(wp.getProduct());
            }
        }
        
        return productsByCategory;
    }
    
    public void addNewProduct() {
        Product p = new Product("CHANGE NAME!", BigDecimal.ONE, "CHANGE DESCRIPTION!", 1, categories.get(0));
        ProductWrapper wp = new ProductWrapper(p);
        wp.setDirty(true);
        this.usedPoducts.add(wp);
        // TODO : save changes to db
        pcs.firePropertyChange(PROP_PRODUCT_ADDED, null, wp);
    }
    
    public void removeProduct(ProductWrapper wp) {
        this.usedPoducts.remove(wp);
        // TODO : save changes to db
        pcs.firePropertyChange(PROP_PRODUCT_REMOVED, null, wp);
    }
    
    public void updateProduct(ProductWrapper wp) {
        
    }
    
    public void markProductAsDirty(ProductWrapper wp) {
        boolean old = wp.isDirty();
        wp.setDirty(true);
        pcs.firePropertyChange(PROP_PRODUCT_MARKED_AS_DIRTY, old, wp.isDirty());
    }
    
    public void markProductForDelete(ProductWrapper wp) {
        boolean old = wp.isDelete();
        wp.setDelete(true);
        pcs.firePropertyChange(PROP_PRODUCT_MARKED_FOR_DELETE, old, wp.isDelete());
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
         this.pcs.addPropertyChangeListener(listener);
     }

     public void removePropertyChangeListener(PropertyChangeListener listener) {
         this.pcs.removePropertyChangeListener(listener);
     }
    
    public void reset() {
        usedPoducts.clear();
        for(ProductWrapper pw : backupProducts) {
            ProductWrapper upw = new ProductWrapper(pw.getProduct());
            usedPoducts.add(upw);
        }
    }
    
    public void save() {
        List<ProductWrapper> forDelete = new ArrayList<>();
        List<ProductWrapper> forUpdate = new ArrayList<>();
        
        for(ProductWrapper pw : usedPoducts) {
            if(pw.isDelete()) {
                forDelete.add(pw);
            } else if(pw.isDirty()) {
                forUpdate.add(pw);
            }
        }
        resetProductsDirtyFlags(forUpdate);
        clearModel();
        // TODO : save changes to db
        
        List<ProductWrapper> list = new ArrayList<>();
        list.addAll(forDelete);
        list.addAll(forUpdate);
        pcs.firePropertyChange(PROP_PRODUCTS_PERSISTED, null, list);
    }
    
    private void resetProductsDirtyFlags(List<ProductWrapper> wDirtyProducts) {
        for (ProductWrapper wp : wDirtyProducts) {
            wp.setDirty(false);
        }
    }
    
    public void clearModel(){
        this.backupProducts.clear();
        this.usedPoducts.clear();
    }
    
    public void toggleEditMode() {
        boolean old = this.editMode;
        this.editMode = !this.editMode;
        pcs.firePropertyChange(PROP_EDITMODE_CHANGED, old, editMode);
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        boolean old = this.editMode;
        this.editMode = editMode;
        pcs.firePropertyChange(PROP_EDITMODE_CHANGED, old, editMode);
    }
    
    private List<ProductWrapper> productListToProductWrapperList(List<Product> products) {
        List<ProductWrapper> productWrappers = new ArrayList<>();
        for(Product p : products) {
            productWrappers.add(new ProductWrapper(p));
        }
        
        return productWrappers;
    }
    
    private List<Product> productWrapperListToProductList(List<ProductWrapper> productWrappers) {
        List<Product> products = new ArrayList<>();
        for(ProductWrapper wp : productWrappers) {
            products.add(wp.getProduct());
        }
        
        return products;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        
    }
}
