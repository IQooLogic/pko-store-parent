package rs.itcentar.pko.store.app;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import rs.itcentar.pko.store.dto.Category;

/**
 *
 * @author milos
 */
public class CategoriesListModel implements ComboBoxModel<Category> {
    
    private List<Category> categories = new ArrayList<>();
    private Category selectedCategory;

    public CategoriesListModel(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public void setSelectedItem(Object cat) {
        this.selectedCategory = (Category) cat;
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedCategory;
    }

    @Override
    public int getSize() {
        return this.categories == null ? 0 : this.categories.size();
    }

    @Override
    public Category getElementAt(int index) {
        return this.categories.get(index);
    }
    
    public int getIndexForCategory(Category cat) {
        for(int i=0;i<categories.size();i++) {
            Category c = categories.get(i);
            if(cat.getName().equals(c.getName())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void addListDataListener(ListDataListener l) {}

    @Override
    public void removeListDataListener(ListDataListener l) {}
}
