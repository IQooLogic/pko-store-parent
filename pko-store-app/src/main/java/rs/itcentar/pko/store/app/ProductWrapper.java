package rs.itcentar.pko.store.app;

import java.math.BigDecimal;
import rs.itcentar.pko.store.dto.Category;
import rs.itcentar.pko.store.dto.Product;

/**
 *
 * @author milos
 */
public class ProductWrapper {
    private Product product;
    private boolean dirty = false;
    private boolean delete = false;

    public ProductWrapper() {}
    
    public ProductWrapper(Product product) {
        this.product = product;
        this.dirty = false;
        this.delete = false;
    }

    public Product getProduct() {
        return product;
    }

    public String getId() {
        return this.product.getId();
    }

    public void setId(String id) {
        this.product.setId(id);
    }

    public String getName() {
        return this.product.getName();
    }

    public void setName(String name) {
        this.product.setName(name);
    }

    public BigDecimal getPrice() {
        return this.product.getPrice();
    }

    public void setPrice(BigDecimal price) {
        this.product.setPrice(price);
    }

    public String getDescription() {
        return this.product.getDescription();
    }

    public void setDescription(String description) {
        this.product.setDescription(description);
    }

    public int getQuantity() {
        return this.product.getQuantity();
    }

    public void setQuantity(int quantity) {
        this.product.setQuantity(quantity);
    }

    public Category getCategory() {
        return this.product.getCategory();
    }

    public void setCategory(Category category) {
        this.product.setCategory(category);
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
